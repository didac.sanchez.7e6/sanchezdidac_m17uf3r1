//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.7.0
//     from Assets/KMS/Imputs/Player.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @Player: IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @Player()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Player"",
    ""maps"": [
        {
            ""name"": ""PlayerActions"",
            ""id"": ""605073e2-bf45-4f55-a0eb-152b3c9927ab"",
            ""actions"": [
                {
                    ""name"": ""Moverse"",
                    ""type"": ""PassThrough"",
                    ""id"": ""b4c6f1b6-f0b2-41cc-ac27-4d6d99f714aa"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Saltar"",
                    ""type"": ""Button"",
                    ""id"": ""2cfa4ea5-0b75-4545-b5b0-256d169d6692"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Agacharse"",
                    ""type"": ""Button"",
                    ""id"": ""f68e6f4b-60e6-488c-a302-e816dc711f75"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Correr"",
                    ""type"": ""Button"",
                    ""id"": ""7db1e549-5a6b-4805-9367-eb2e6c973e7b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""61909aea-ade9-420a-ac94-1e360d3c86d2"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moverse"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""340cb0b0-db3e-4a72-868a-7425bb37722b"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""022f1d33-6a90-4fb8-bc5b-beb6bdd9a428"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""cf572103-cd6d-4f76-b331-b4817bfddac9"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""49bb3bc4-5d8f-4c5e-a17b-58ff78785d31"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""7dfc5580-b735-4fcf-a3f4-5160bee551a9"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Saltar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fee14afb-c2ed-4cae-a976-440725c5e402"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Agacharse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9ad0cbb4-fe3c-4262-898d-68bf5d42ffd9"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Correr"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerActions
        m_PlayerActions = asset.FindActionMap("PlayerActions", throwIfNotFound: true);
        m_PlayerActions_Moverse = m_PlayerActions.FindAction("Moverse", throwIfNotFound: true);
        m_PlayerActions_Saltar = m_PlayerActions.FindAction("Saltar", throwIfNotFound: true);
        m_PlayerActions_Agacharse = m_PlayerActions.FindAction("Agacharse", throwIfNotFound: true);
        m_PlayerActions_Correr = m_PlayerActions.FindAction("Correr", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }

    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // PlayerActions
    private readonly InputActionMap m_PlayerActions;
    private List<IPlayerActionsActions> m_PlayerActionsActionsCallbackInterfaces = new List<IPlayerActionsActions>();
    private readonly InputAction m_PlayerActions_Moverse;
    private readonly InputAction m_PlayerActions_Saltar;
    private readonly InputAction m_PlayerActions_Agacharse;
    private readonly InputAction m_PlayerActions_Correr;
    public struct PlayerActionsActions
    {
        private @Player m_Wrapper;
        public PlayerActionsActions(@Player wrapper) { m_Wrapper = wrapper; }
        public InputAction @Moverse => m_Wrapper.m_PlayerActions_Moverse;
        public InputAction @Saltar => m_Wrapper.m_PlayerActions_Saltar;
        public InputAction @Agacharse => m_Wrapper.m_PlayerActions_Agacharse;
        public InputAction @Correr => m_Wrapper.m_PlayerActions_Correr;
        public InputActionMap Get() { return m_Wrapper.m_PlayerActions; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActionsActions set) { return set.Get(); }
        public void AddCallbacks(IPlayerActionsActions instance)
        {
            if (instance == null || m_Wrapper.m_PlayerActionsActionsCallbackInterfaces.Contains(instance)) return;
            m_Wrapper.m_PlayerActionsActionsCallbackInterfaces.Add(instance);
            @Moverse.started += instance.OnMoverse;
            @Moverse.performed += instance.OnMoverse;
            @Moverse.canceled += instance.OnMoverse;
            @Saltar.started += instance.OnSaltar;
            @Saltar.performed += instance.OnSaltar;
            @Saltar.canceled += instance.OnSaltar;
            @Agacharse.started += instance.OnAgacharse;
            @Agacharse.performed += instance.OnAgacharse;
            @Agacharse.canceled += instance.OnAgacharse;
            @Correr.started += instance.OnCorrer;
            @Correr.performed += instance.OnCorrer;
            @Correr.canceled += instance.OnCorrer;
        }

        private void UnregisterCallbacks(IPlayerActionsActions instance)
        {
            @Moverse.started -= instance.OnMoverse;
            @Moverse.performed -= instance.OnMoverse;
            @Moverse.canceled -= instance.OnMoverse;
            @Saltar.started -= instance.OnSaltar;
            @Saltar.performed -= instance.OnSaltar;
            @Saltar.canceled -= instance.OnSaltar;
            @Agacharse.started -= instance.OnAgacharse;
            @Agacharse.performed -= instance.OnAgacharse;
            @Agacharse.canceled -= instance.OnAgacharse;
            @Correr.started -= instance.OnCorrer;
            @Correr.performed -= instance.OnCorrer;
            @Correr.canceled -= instance.OnCorrer;
        }

        public void RemoveCallbacks(IPlayerActionsActions instance)
        {
            if (m_Wrapper.m_PlayerActionsActionsCallbackInterfaces.Remove(instance))
                UnregisterCallbacks(instance);
        }

        public void SetCallbacks(IPlayerActionsActions instance)
        {
            foreach (var item in m_Wrapper.m_PlayerActionsActionsCallbackInterfaces)
                UnregisterCallbacks(item);
            m_Wrapper.m_PlayerActionsActionsCallbackInterfaces.Clear();
            AddCallbacks(instance);
        }
    }
    public PlayerActionsActions @PlayerActions => new PlayerActionsActions(this);
    public interface IPlayerActionsActions
    {
        void OnMoverse(InputAction.CallbackContext context);
        void OnSaltar(InputAction.CallbackContext context);
        void OnAgacharse(InputAction.CallbackContext context);
        void OnCorrer(InputAction.CallbackContext context);
    }
}
