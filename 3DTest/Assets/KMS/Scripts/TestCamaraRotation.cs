using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TestCamaraRotation : MonoBehaviour
{
    public GameObject target;
    public Rigidbody rb;
    private float velocity = 0f;
    private float rotation = 0f;

    private void OnEnable()
    {
        ActionsCamara.OnMoveRight += RotateCamara;
        ActionsCamara.OnMoveLeft += RotateCamaraOtherWey;
        ActionsCamara.OnStopRotate += StopRotate;
    }
    public void RotateCamara()
    {
        velocity = 70f;
        rotation = -1f;
    }
    public void RotateCamaraOtherWey()
    {
        velocity = 70f;
        rotation = 1f;
    }
    public void StopRotate()
    {
        velocity = 0f;
        rotation = 0f;
    }
    private void Update()
    {
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(rb.position + new Vector3(0f, 1f, 0f) - transform.position), 70f * Time.deltaTime);
        //transform.RotateAround(rb.position, Vector3.up * rotation, velocity * Time.deltaTime);
        rb.angularVelocity = new Vector3(0f, (velocity) * rotation * Time.deltaTime, 0f);
    }
}
