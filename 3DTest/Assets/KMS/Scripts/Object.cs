using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public Vector3 rot;
    public Vector3 pos;
    public string nameS;
    public GameObject place;
    public bool picked = false;
    public Rigidbody rb;
    private void OnTriggerEnter(Collider other)
    {
        if (!picked)
        {
            Object obj = Instantiate(this, place.transform);
            Destroy(obj.GetComponent<Rigidbody>());
            obj.gameObject.name = nameS;
            obj.transform.localPosition = pos;
            obj.transform.localEulerAngles = rot;
            obj.picked = true;
            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        if (!picked)
        {
            rb.angularVelocity = new Vector3(0f, (10f) * 4f * Time.deltaTime, 0f);
        }
    }

}
