using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using static UnityEngine.Rendering.HDROutputUtils;

public class Move : MonoBehaviour
{
    private Rigidbody _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void MoveThing(Vector3 direction, float velocity)
    {
        Vector3 move = direction.z * transform.forward + direction.x * transform.right;
        _rb.velocity = move * velocity;
        if (_rb.velocity.Equals(Vector3.zero))
        {
            AnimatorManager.Instance.NoAnimation("Walk");
        }
    }
    public void Jump(float force)
    {
        _rb.AddForce(Vector3.up * force);
    }
    private void Update()
    {
        AnimatorManager.Instance.Velocity(_rb.velocity.y);
    }
}
