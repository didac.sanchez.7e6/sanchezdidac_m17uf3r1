using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamChangesPT : MonoBehaviour
{
    public GameObject primeraCamara;
    public GameObject trceraCamara;
    public void OnEnable()
    {
        ActionsCamara.OnChangeCam += ChangeCam;
    }
    public void ChangeCam()
    {
        trceraCamara.SetActive(!trceraCamara.activeSelf);
        AnimatorManager.Instance.ChangeCam();
        primeraCamara.SetActive(!primeraCamara.activeSelf);
    }
}
