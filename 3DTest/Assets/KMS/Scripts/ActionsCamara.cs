using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ActionsCamara : MonoBehaviour
{ 
    public static ActionsCamara Instance;
    public PlayerInput camara;
    public static event Action OnMoveRight = delegate { };
    public static event Action OnMoveLeft = delegate { };
    public static event Action OnStopRotate = delegate { };
    public static event Action OnChangeCam = delegate { };
    private void Awake()
    {
        camara = GetComponent<PlayerInput>();
    }
    private void OnEnable()
    {
        camara.actions["MoveRight"].started += MoveR;
        camara.actions["MoveLeft"].started += MoveL;
        camara.actions["MoveRight"].canceled += Stop;
        camara.actions["MoveLeft"].canceled += Stop;
        camara.actions["CamaraChange"].canceled += Change;
    }
    public void Change(InputAction.CallbackContext ctx)
    {
        OnChangeCam.Invoke();
    }
    public void MoveR(InputAction.CallbackContext ctx)
    {
        OnMoveRight.Invoke();
    }
    public void MoveL(InputAction.CallbackContext ctx)
    {
        OnMoveLeft.Invoke();
    }
    public void Stop(InputAction.CallbackContext ctx)
    {
        OnStopRotate.Invoke();
    }
}
