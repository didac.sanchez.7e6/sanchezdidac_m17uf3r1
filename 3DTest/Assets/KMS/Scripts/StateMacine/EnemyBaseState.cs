﻿public abstract partial class EnemyBaseState : IState
{
    protected readonly Enemy enemy;

    protected const float crossFadeDuration = 0.1f;

    protected EnemyBaseState(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public virtual void FixedUpdate()
    {
        
    }

    public virtual void OnEnter()
    {
        
    }

    public virtual void OnExit()
    {
        
    }

    public virtual void Update()
    {
        
    }
}