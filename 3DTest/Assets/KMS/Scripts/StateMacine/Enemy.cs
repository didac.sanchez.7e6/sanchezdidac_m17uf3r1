using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PlayerDetection))]
public partial class Enemy : MonoBehaviour
{
    [SerializeField] NavMeshAgent agent;

    [SerializeField] PlayerDetection playerDetection;

    [SerializeField] float wanderRadius = 10f;
    [SerializeField] float timeBetweenAttacks = 1f;

    [SerializeField] float maxLP = 10f;
    [SerializeField] float actualLP;

    StateMachine stateMachine;

    CountdownTimer attackTimer;

    private void Start()
    {
        attackTimer = new CountdownTimer(timeBetweenAttacks);
        stateMachine = new StateMachine();
        actualLP = maxLP;

        var wanderState = new EnemyWanderState(this, agent, wanderRadius);
        var chaseState = new EnemyChaseState(this, agent, playerDetection.Player);
        var attackState = new EnemyAttackState(this, agent, playerDetection.Player);
        var fleeState = new EnemyFleeState(this, agent, playerDetection.Player);


        At(wanderState, chaseState, new FuncPredicate(() => playerDetection.CanDetectPlayer()));
        At(chaseState, wanderState, new FuncPredicate(() => !playerDetection.CanDetectPlayer()));
        At(chaseState, attackState, new FuncPredicate(() => !playerDetection.CanAttack()));
        At(attackState, chaseState, new FuncPredicate(() => !playerDetection.CanAttack()));
        At(wanderState, fleeState, new FuncPredicate(() => playerDetection.CanDetectPlayer() && actualLP <= (maxLP / 2)));
        At(chaseState, wanderState, new FuncPredicate(() => !playerDetection.CanDetectPlayer()));
        At(chaseState, fleeState, new FuncPredicate(() => actualLP <= (maxLP / 2)));

        stateMachine.SetState(wanderState);
    }

    void At(IState from, IState to, IPredicate condition) => stateMachine.AddTransition(from, to, condition);
    void Any(IState to, IPredicate condition) => stateMachine.AddAnyTransition(to, condition);

    private void Update()
    {
        stateMachine.Update();
        attackTimer.Tick(Time.deltaTime);
    }
    private void FixedUpdate()
    {
        stateMachine.FixedUpdate();
    }
    public void Attack()
    {
        if (attackTimer.IsRunning) return;

        attackTimer.Start();
        Debug.Log("Attacking");
    }
}
