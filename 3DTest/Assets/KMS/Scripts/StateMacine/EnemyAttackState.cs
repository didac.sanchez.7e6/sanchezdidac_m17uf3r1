﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyAttackState : EnemyBaseState
{
    private NavMeshAgent agent;
    private Transform player;

    public EnemyAttackState(Enemy enemy, NavMeshAgent agent, Transform player) : base(enemy)
    {
        this.agent = agent;
        this.player = player;
    }
    public override void OnEnter()
    {
        Debug.Log("Attack");
    }

    public override void Update()
    {
        agent.SetDestination(player.position);
        enemy.Attack();
    }
}

