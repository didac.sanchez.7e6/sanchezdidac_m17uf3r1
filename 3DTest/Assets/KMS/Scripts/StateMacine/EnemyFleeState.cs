﻿using UnityEngine.AI;
using UnityEngine;

public class EnemyFleeState : EnemyBaseState
{
    private NavMeshAgent agent;
    private Transform player;

    public EnemyFleeState(Enemy enemy, NavMeshAgent agent, Transform player) : base(enemy)
    {
        this.agent = agent;
        this.player = player;
    }

    public override void OnEnter()
    {
        Debug.Log("Flee");
    }

    public override void Update()
    {
        agent.SetDestination(enemy.transform.position - player.position);
    }
}

