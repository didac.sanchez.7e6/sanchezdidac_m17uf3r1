﻿
using UnityEngine;
using UnityEngine.AI;

public class EnemyWanderState : EnemyBaseState
{
    private NavMeshAgent agent;
    private Vector3 startPoint;
    private float wanderRadius;

    public EnemyWanderState(Enemy enemy,NavMeshAgent agent, float wanderRadius) : base(enemy)
    {
        this.agent = agent;
        this.startPoint = enemy.transform.position;
        this.wanderRadius = wanderRadius;
    }
    public override void OnEnter()
    {
        Debug.Log("Wander");
    }
    public override void Update()
    {
        if (HasReachedDestination())
        {
            var randomDirection = Random.insideUnitSphere * wanderRadius;
            randomDirection += startPoint;
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection,out hit, wanderRadius, 1);
            var finalPosition = hit.position;
            agent.SetDestination(finalPosition);
        }
    }
    bool HasReachedDestination()
    {
        return agent.remainingDistance <= agent.stoppingDistance
               && !agent.pathPending
               && (!agent.hasPath || agent.velocity.sqrMagnitude == 0);
    }
}
