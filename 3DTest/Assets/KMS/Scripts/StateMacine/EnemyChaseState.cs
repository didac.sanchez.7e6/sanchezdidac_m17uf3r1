﻿
using Unity.AI.Navigation.Editor;
using UnityEngine;
using UnityEngine.AI;
public class EnemyChaseState : EnemyBaseState
{
    private NavMeshAgent agent;
    private Transform player;

    public EnemyChaseState(Enemy enemy, NavMeshAgent agent, Transform player) : base(enemy)
    {
        this.agent = agent;
        this.player = player;
    }

    public override void OnEnter()
    {
        Debug.Log("Chase");
    }

    public override void Update()
    {
        agent.SetDestination(player.position);
    }
}

