﻿using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    [SerializeField] float detectionAngle = 60f;
    [SerializeField] float detectionRadius = 25f;
    [SerializeField] float innerDetectionRadius = 10f;
    [SerializeField] float detectionCooldown = 1f;
    [SerializeField] float attackRange = 2f;


    public Transform Player { get; private set; }
    CountdownTimer detectionTimer;

    IDetectionStrategy detectionStrategy;

    private void Start()
    {
        detectionTimer = new CountdownTimer(detectionCooldown);
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        detectionStrategy = new ConeDetectionStrategy(detectionAngle, detectionRadius, innerDetectionRadius);
    }
    private void Update() => detectionTimer.Tick(Time.deltaTime);

    public bool CanDetectPlayer()
    {
        return detectionTimer.IsRunning || detectionStrategy.Execute(Player,transform,detectionTimer);
    }

    public bool CanAttack()
    {
        var directionToPlayer = Player.position - transform.position;
        return directionToPlayer.magnitude <= attackRange;
    }
}
