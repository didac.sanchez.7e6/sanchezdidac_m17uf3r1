using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    public static AnimatorManager Instance;
    public Animator animator;
    private void Awake()
    {
        Instance = this;
    }
    public void Animation(string animation)
    {
        animator.SetBool(animation, true);
    }
    public void NoAnimation(string animation)
    {
        animator.SetBool(animation, false);
    }
    public void Velocity(float vel)
    {
        animator.SetFloat("VelocityY", vel);
    }
    public void ChangeCam()
    {
        animator.SetBool("Aim", !animator.GetBool("Aim"));
    }
}
