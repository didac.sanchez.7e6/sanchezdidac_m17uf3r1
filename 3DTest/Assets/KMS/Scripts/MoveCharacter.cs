using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacter : Move
{
    public float velocity = 20f;
    public float forceJump = 350f;
    public bool floor = true;
    private void OnEnable()
    {
        ActionPlayer.OnMove += Move;
        ActionPlayer.OnCorrer += SpeedUp;
        ActionPlayer.OnAgachar += SpeedDown;
        ActionPlayer.OnNormalSpeed += SpeedNormal;
        ActionPlayer.OnSaltar += Saltar;
        ActionPlayer.OnStopPlayer += StopMove;
    }

    public void StopMove()
    {
        Debug.Log("b");
        AnimatorManager.Instance.NoAnimation("Walk");
    }

    public void Saltar()
    {
        if (floor)
        {
            floor = !floor;
            AnimatorManager.Instance.Animation("Jump");
            Jump(forceJump);
        }
        
    }
    private void Move(Vector2 direction)
    {
        MoveThing(new Vector3(direction.x, 0f, direction.y).normalized, velocity);
        AnimatorManager.Instance.Animation("Walk");
    }
    private void SpeedUp()
    {
        velocity = 25f;
        AnimatorManager.Instance.Animation("Run");
    }
    private void SpeedDown()
    {
        velocity = 10f;
        AnimatorManager.Instance.Animation("Crouch");
    }
    private void SpeedNormal()
    {
        velocity = 20f;
        AnimatorManager.Instance.NoAnimation("Run");
        AnimatorManager.Instance.NoAnimation("Crouch");
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!floor)
        {
            floor = !floor;
            AnimatorManager.Instance.NoAnimation("Jump");
        }
        
    }
}
