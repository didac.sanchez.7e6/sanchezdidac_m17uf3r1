using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ActionPlayer : MonoBehaviour
{
    public static ActionPlayer Instance;
    public PlayerInput player;
    public static event Action<Vector2> OnMove = delegate { };
    public static event Action OnSaltar = delegate { };
    public static event Action OnAgachar = delegate { };
    public static event Action OnCorrer = delegate { };
    public static event Action OnNormalSpeed = delegate { };
    public static event Action OnStopPlayer = delegate { };
    private void Awake()
    {
        player = GetComponent<PlayerInput>();
    }
    private void OnEnable()
    {
        player.actions["Moverse"].performed += MovePlayer;
        player.actions["Moverse"].canceled += StopPlayer;
        player.actions["Agacharse"].performed += SpeedDown;
        player.actions["Agacharse"].canceled += NormalSpeed;
        player.actions["Correr"].performed += SpeedUp;
        player.actions["Correr"].canceled += NormalSpeed;
        player.actions["Saltar"].performed += Jump;
    }
    private void Jump(InputAction.CallbackContext ctx)
    {
        OnSaltar.Invoke(); 
    }
    private void StopPlayer(InputAction.CallbackContext ctx)
    {
        OnStopPlayer.Invoke();
    }
    private void SpeedUp(InputAction.CallbackContext ctx)
    {
        OnCorrer.Invoke(); 
    }

    private void SpeedDown(InputAction.CallbackContext ctx)
    {
        OnAgachar.Invoke();
    }

    private void NormalSpeed(InputAction.CallbackContext ctx)
    {
        OnNormalSpeed.Invoke();
    }

    public void MovePlayer(InputAction.CallbackContext ctx)
    {
        OnMove.Invoke(ctx.ReadValue<Vector2>());
    }
}
